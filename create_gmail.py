import string
import time
import traceback
import urllib
import random
import pyautogui
from selenium import webdriver
import os
from fake_useragent import UserAgent
from webdriver_manager.chrome import ChromeDriverManager
from urllib.request import urlopen
import csv
class create_gmail():
	def __init__(self):
		self.ua = UserAgent()
		self.init_browser()

	def is_bad_proxy(self, pip):
		try:
			r=urllib.request
			r.ProxyHandler({"http" : pip})
			r.urlopen(
				"https://whatismyipaddress.com/"
			)
		except Exception as e:
			return 1
		return 0

	def get_strong_password(self):
		sp=['#','@','*','-']
		p = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits+string.punctuation, k=9))
		return p+random.choices(sp)[0]
	def get_random_user_name(self):
		user_name=[]
		with open('person.csv', mode='r') as file:
			csvFile = csv.reader(file)
			for lines in csvFile:
				if lines[0] == 'Index' or lines[2] == '': continue
				user_name.append(lines[2].strip())
		return random.choices(user_name)
	def save_data_in_csv(self,name='',mail='',password='',relation_mail='',ip='',port='',proxy='my',dob='',gender='', filename='r.csv'):
		with open(filename, 'a') as csvfile:
			# creating a csv writer object
			csvwriter = csv.writer(csvfile)
			csvwriter.writerow([name,'',mail,relation_mail,password,ip,port,proxy,dob,gender ])

	def init_browser(self):
		user_name = self.get_random_user_name()[0]
		chromedriver = "chromedriver.exe"
		os.environ["webdriver.chrome.driver"] = chromedriver
		chrome_options = webdriver.ChromeOptions()
		prefs = {"profile.default_content_setting_values.notifications": 2,
				 "profile.default_content_settings.popups": 0}
		chrome_options.add_experimental_option("prefs", prefs)
		chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
		chrome_options.add_experimental_option('useAutomationExtension', False)
		chrome_options.add_argument("--disable-notifications")
		chrome_options.add_argument("--allow-running-insecure-content")
		ua = self.ua.random
		print(f"Agent {ua} ")
		# chrome_options.add_argument(f"user-agent={ua}")
		# chrome_options.add_argument('--proxy-server=socks5://127.0.0.1:9150')
		self.app_driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
		self.app_driver.delete_all_cookies()
		# self.app_driver.execute_cdp_cmd('Network.setUserAgentOverride', {
		# 	"userAgent": ua,
		# 	"platform": ua.split(" ")[1].replace("(","").replace(";","")})
		self.app_driver.set_window_size(1300, 720)
		self.app_driver.get("https://accounts.google.com/signup")
		time.sleep(5)
		self.set_user_name(user_name)
		month,day,year,gender = self.set_dob(user_name)

		if month =="":
			print(f"error in creating gmail...")
			time.sleep(60)
		time.sleep(3)
		mail = self.set_mail()
		random_password=''
		while True:
			try:
				random_password = self.get_strong_password()
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div/div[1]/div/div[1]/div/div[1]/input").click()
				time.sleep(1)
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div/div[1]/div/div[1]/div/div[1]/input").click()
				time.sleep(1)
				password_field = self.app_driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div/div[1]/div/div[1]/div/div[1]/input")
				for p in random_password:
					password_field.send_keys(p)
				time.sleep(2)
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input").click()
				confirm_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input")
				for p in random_password:
					confirm_field.send_keys(p)
				time.sleep(2)
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div/button").click()
				if pyautogui.locateOnScreen('gmail_img/mail_exists.png') != None:
					time.sleep(1)
					continue
				else:
					break
			except Exception as e:
				print(f"error in password field")
				time.sleep(1)
		print(f"mail {mail} random_password {random_password} user_name {user_name} month,day,year,gender {month},{day},{year},{gender}")
		time.sleep(50)
		self.save_data_in_csv(name=user_name,mail=mail,password=random_password,relation_mail=mail,ip='',port='',proxy='my',dob=str(month)+'/'+str(day)+'/'+str(year),gender=gender)

		time.sleep(5)
		# asyncio.ensure_future(self.generate_user())
		# threading.Thread(target=self.generate_user).start()
	def set_user_name(self,user_name):
		self.app_driver.find_element_by_xpath(
			"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div[1]/div/div[1]/div/div[1]/input").click()
		time.sleep(1)
		input_box = self.app_driver.find_element_by_xpath(
			"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div[1]/div/div[1]/div/div[1]/input")
		input_box.send_keys(user_name)
		time.sleep(1)
		self.app_driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div/button").click()
		time.sleep(3)

	def set_mail(self):
		mail = ""
		for a in range(0,10):
			if pyautogui.locateOnScreen('gmail_img/mail_enter_field.png', confidence=0.8) == None:
				time.sleep(1)
				continue
			else:
				break
		select_check = False
		if pyautogui.locateOnScreen('gmail_img/mail_enter_field.png', confidence=0.8) == None:
			select_check = True
			try:
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/span/div[1]/div/div[2]/div[1]/div").click()
			except Exception as e:
				select_check = False


		if select_check:
			self.app_driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/span/div[1]/div/div[2]/div[1]/div").click()
			mail = self.app_driver.find_element_by_xpath(
				"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/span/div[1]/div/div[2]/div[1]/div").text
			time.sleep(1)
			print(f"mail found {mail}")
			self.app_driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div/button").click()
		else:
			while True:
				mail_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div[1]/div/div[1]/div/div[1]/input")
				mail = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=9))
				mail_field.send_keys(mail)
				time.sleep(2)
				if pyautogui.locateOnScreen('gmail_img/mail_user_field.png', confidence=0.8) != None:
					time.sleep(1)
					continue
				else:
					self.app_driver.find_element_by_xpath(
						"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div/button").click()
					if pyautogui.locateOnScreen('gmail_img/mail_exists.png', confidence=0.8) != None:
						time.sleep(1)
						continue
					else:
						break
		return mail


	def set_dob(self,user_name):
		month = random.choices(['jan','feb','march','april','may','june','july','aug','sep','oct','nov','dec'])[0]
		day = random.choices([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24])[0]
		year = random.choices([1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002])[0]
		gender= random.choices(['male','female'])[0]
		try:
			first_month = False
			try:
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div[1]/div/div[1]/input").click()
			except Exception as e:
				first_month=True
			time.sleep(1)
			if first_month:
				try:
					self.app_driver.find_element_by_xpath(
						"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div[2]/select").click()
				except Exception as e:
					self.app_driver.find_element_by_xpath(
						"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div[2]/select").click()
				time.sleep(1)
				while True:
					month = random.choices(['jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'])[0]
					location = pyautogui.locateOnScreen('gmail_img/' + month + '.png', confidence=0.8)
					if location == None:
						self.app_driver.find_element_by_xpath(
							"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div[2]/select").click()
						time.sleep(1)
						print(f"month not match {month} location {location} ")
						continue
					pyautogui.click(location)
					break
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[2]/div/div/div[1]/div/div[1]/input").click()
				text_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[2]/div/div/div[1]/div/div[1]/input")
				text_field.send_keys(day)
				time.sleep(1)
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/input").click()
				time.sleep(1)
				text_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/input")
				text_field.send_keys(year)
				time.sleep(1)

			else:
				text_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div[1]/div/div[1]/input")
				text_field.send_keys(day)
				time.sleep(1)
				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[2]/div/div/div[2]/select").click()
				time.sleep(1)
				while True:
					month = random.choices(['jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'])[0]
					location = pyautogui.locateOnScreen('gmail_img/' + month + '.png', confidence=0.8)
					if location == None:
						self.app_driver.find_element_by_xpath(
							"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[2]/div/div/div[2]/select").click()
						time.sleep(1)
						print(f"month not match {month} location {location} ")
						continue
					pyautogui.click(location)
					break

				self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/input").click()
				time.sleep(1)
				text_field = self.app_driver.find_element_by_xpath(
					"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/input")
				text_field.send_keys(year)
				time.sleep(1)
			self.app_driver.find_element_by_xpath(
				"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div[1]/div/div[2]/select").click()
			time.sleep(2)
			while True:
				gender = random.choices(['male', 'female'])[0]
				location = pyautogui.locateOnScreen('gmail_img/' + gender + '.png', confidence=0.8)
				if location == None:
					self.app_driver.find_element_by_xpath(
						"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div[1]/div/div[2]/select").click()
					time.sleep(1)
					print(f"gender {gender} not match {location} ")
					continue
				pyautogui.click(location)
				break
			time.sleep(1)
			self.app_driver.find_element_by_xpath(
				"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div/div/button").click()

			return month,day,year,gender

		except Exception as e:
			print(f"Dob field Error {traceback.format_exc()}")
			return "","","",""

	def generate_user(self):
		time.sleep(1)
		while True:
			print("dsf")
			time.sleep(5)


create_gmail()
